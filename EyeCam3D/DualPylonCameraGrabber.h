#pragma once
#include <memory>
#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

class DualPylonCameraGrabber
{

public:
	DualPylonCameraGrabber();
	~DualPylonCameraGrabber();
	void ReverseCameras();
	void GrabSBSImage();
	bool ResetCameras();
	bool ResizeImages(int64_t width = 0, int64_t height = 0);
	const uint8_t * GetLatestSBSFrame();

private:
	std::unique_ptr<Pylon::CInstantCameraArray> cameras;
	std::shared_ptr<uint8_t> framePairRollingBuffer;
	unsigned int latestFrameWidth = 0;
	unsigned int latestFrameHeight = 0;
	unsigned int reverseCameras = 0;
	bool camerasInitialized = false;
	unsigned int frameDelay_ms = (unsigned int)ceil(1000.0 / 60.0);
	bool running = false;
	unsigned int bufferIterator = 1;
	unsigned int bufferFrameCount = 3;
	Pylon::CGrabResultPtr ptrGrabResult;
	bool useHalfSBS = true;
	int bytesPerPixel = 3;

	bool InitializeCameras();
	int64_t Adjust(int64_t val, int64_t minimum, int64_t maximum, int64_t inc);
};

