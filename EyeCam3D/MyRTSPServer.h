#pragma once
#include <NetCommon.h>
#include "MJPEGVideoSource.h"

class MyRTSPServer
{
public:
	MyRTSPServer();
	~MyRTSPServer();

	void SetNewBuffer(u_int8_t* bufptr, u_int64_t bufSize);



private:
	u_int8_t* jpegBuffer;
	u_int64_t jpegBufferSize;
	char watchVariable = 0;
	MJPEGVideoSource* videoSource;
};

